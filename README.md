# jupyternotebook

This project/repository is used to create a Gesis Notebook for demonstrating using Jupyternote book to analysis raw data.

## Getting started

To start the Gesis notebook use the URL.

https://notebooks.gesis.org/binder/v2/gl/yudonggesis%2Fjupyternotebook/HEAD

Or alternativelly, you can do the binding manually, follow the steps below: 

1. Go to the Gesis binder page : **https://notebooks.gesis.org/binder/**
2. In the page, change **GitHub** to **GitLab.com**
3. Input the notebook's Gitlab repository URL i.e. **https://gitlab.com/yudonggesis/jupyternotebook**
4. Click button **"launch"**
5. An instance of the notebook will start automatically on the browser.

Yudong